class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :phone
      t.string :number_permie
      t.string :password
      t.string :password_confirmation
      t.string :street
      t.string :city
      t.string :zipcode
      t.string :fidelity
      t.references :role, foreign_key: true , default: 2
      t.timestamps
    end
  end
end
