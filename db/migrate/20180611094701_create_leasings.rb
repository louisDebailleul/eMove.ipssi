
class CreateLeasings < ActiveRecord::Migration[5.1]
  def change
    create_table :leasings do |t|
      t.date :beginning_date
      t.date :end_date
      t.string :limit_km
      t.string :penalty , default: 0
      t.integer :day
      t.boolean :approved , default: false
      t.references :user
      t.references :vehicle
      t.timestamps
    end
  end
end