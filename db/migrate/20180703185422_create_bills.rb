class CreateBills < ActiveRecord::Migration[5.1]
  def change
    create_table :bills do |t|
      t.integer :num_bill
      t.date :deadline
      t.boolean :paid
      t.float :ttc
      t.float :ht
      t.float :total
      t.references :leasing
      t.timestamps
    end
  end
end
