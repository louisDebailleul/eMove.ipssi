class CreateVehicles < ActiveRecord::Migration[5.1]
  def change
    create_table :vehicles do |t|
      t.string :num_serial
      t.string :color
      t.string :numberplate
      t.string :num_km
      t.text :description
      t.date   :buying_date
      t.string :buying_price
      t.string :photo
      t.float :price
      t.references :model
      t.references :type
      t.timestamps
    end
  end
end
