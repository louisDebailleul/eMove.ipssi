# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

%w[admin client].each do |role|
  Role.create!(name: role)
end
type = [
  {
   name: 'Scooter'
        },
        {
    name: 'Voiture'
        }
]
Type.create! type
users = [
  {
      first_name:Faker::Name.name,
      last_name:Faker::Name.name,
      email: "admin@gmail.com",
      phone:  Faker::Number.between(1000000000,9999999999),
      number_permie:  Faker::Number.between(1000000000,9999999999),
      password: '123456',
      password_confirmation: '123456',
      street: Faker::Name.name,
      city: Faker::Name.name,
      zipcode: Faker::Number.between(10000,99999),
      fidelity: Faker::Number.between(0,100),
      role_id: 1
  },
  {

    first_name:Faker::Name.name,
    last_name:Faker::Name.name,
    email: "admin2@gmail.com",
    phone:  Faker::Number.between(1000000000,9999999999),
    number_permie:  Faker::Number.between(1000000000,9999999999),
    password: '123456',
    password_confirmation: '123456',
    street: Faker::Name.name,
    city: Faker::Name.name,
    zipcode: Faker::Number.between(10000,99999),
    fidelity: Faker::Number.between(0,100),
    role_id: 1
  },
  {

    first_name:Faker::Name.name,
    last_name:Faker::Name.name,
    email: "client@gmail.com",
    phone:  Faker::Number.between(1000000000,9999999999),
    number_permie:  Faker::Number.between(1000000000,9999999999),
    password: '123456',
    password_confirmation: '123456',
    street: Faker::Name.name,
    city: Faker::Name.name,
    zipcode: Faker::Number.between(10000,99999),
    fidelity: Faker::Number.between(0,100),
    role_id: 2
  }
]

User.create! users

5.times.each do
  Brand.create!(
    name: Faker::Name.name,
    logo:  Rails.root.join(Dir.glob('db/logo/*').sample).open,
    history: Faker::Lorem.paragraph_by_chars(256, false)
  )
end

30.times.each do
  brand = Brand.order('rand()').first
  Model.create!(
    name: Faker::Name.name,
    brand: brand
  )
end

40.times.each do
  model = Model.order('rand()').first
  type = Type.order('rand()').first
  Vehicle.create!(
  num_serial: Faker::Number.between(8000,200000),
  color:'Noir',
  price: Faker::Number.between(20,100),
  description:Faker::Lorem.paragraph,
  numberplate: Faker::Number.between(2,9),
  num_km: Faker::Number.between(8000,200000),
  buying_date:'20/03/2010',
  photo: Rails.root.join(Dir.glob('db/photo/*').sample).open,
  buying_price: Faker::Number.between(8000,200000),
  type: type,
  model: model,
  )
end
30.times.each do
  user = User.order('rand()').first
  vehicle = Vehicle.order('rand()').first
  Leasing.create!(
    beginning_date: '10/07/2018',
    end_date: '20/07/2018',
    limit_km: Faker::Number.between(10000,99999),
    penalty: Faker::Number.between(0,3),
    day: Faker::Number.between(1,30),
    approved: Faker::Number.between(0,1),
    user: user,
    vehicle: vehicle
  )
end

30.times.each do
  leasing = Leasing.order('rand()').first
  Bill.create!(
    num_bill: Faker::Number.between(1,100),
    deadline: '20/07/2018',
    ttc: Faker::Number.between(1,100),
    ht: Faker::Number.between(1,100),
    total: Faker::Number.between(1,100),
    paid: Faker::Boolean.boolean,
    leasing: leasing
  )
end
