Rails.application.routes.draw do

  resources :types
  devise_for :users

  root to: 'static#home'

  resources :leasings
  resources :bills
  resources :brands
  resources :vehicles
  resources :models
  resources :users
  get '/louer/vehicule/:id', to: 'leasings#new', as:'louer'
  get '/demande/vehicule/:id', to: 'leasings#create', as:'demande'

  namespace :ad do
    resources :bills
    resources :leasings
    resources :brands
    resources :vehicles
    resources :models
    resources :users

    root :to => "static#home"
  end

end
