class ApplicationController < ActionController::Base
  protect_from_forgery with: :reset_session

  include Pundit
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  protected

  def user_not_authorized
    flash[:alert] = 'Veuillez vous inscrire pour accéder à cette fonction'
    redirect_to(request.referrer || root_url)
  end

  def after_sign_in_path_for(resource)
    if resource.admin?
      ad_root_url
    else
      user_url(current_user)
    end
  end

  def instrument(name, options)
    ActiveSupport::Notifications.instrument name, options
  end
end
