module Ad
  class StaticController < BaseController
    def home
      @bills = Bill.order(:created_at).limit(5)
      @leasings = Leasing.order(:created_at).limit(5)
    end
  end
end