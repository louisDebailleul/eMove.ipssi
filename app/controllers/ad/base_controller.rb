# frozen_string_literal: true

module Ad
  # for layout
  class BaseController < ::ApplicationController
    before_action :authenticate_admin
    layout 'ad/application'
   # before_action :set_access
    def authenticate_admin
      if !user_signed_in? || current_user.role.name != 'admin'
        redirect_to new_user_path, danger: "Vous n'avez pas le droit d'accéder à cette page"
      end
    end
  end
end