module Ad
    class LeasingsController < BaseController
      before_action :set_leasing, only: [:show, :edit, :update, :destroy]

      # GET /leasings
      # GET /leasings.json
      def index
        #page params[:page] donne la page pour la pagination

        @q = Leasing.order(created_at: :DESC).ransack(params[:q])
        @leasings = @q.result.includes(:user, :vehicle).page params[:page]
      end

      # GET /leasings/1
      # GET /leasings/1.json
      def show
      end

      # GET /leasings/new
      def new
        @leasing = Leasing.new
      end

      # GET /leasings/1/edit
      def edit
      end

      # POST /leasings
      # POST /leasings.json
      def create
        @leasing = Leasing.new(leasing_params)

        respond_to do |format|
          if @leasing.save
            format.html { redirect_to @leasing, notice: 'Leasing was successfully created.' }
            format.json { render :show, status: :created, location: @leasing }
          else
            format.html { render :new }
            format.json { render json: @leasing.errors, status: :unprocessable_entity }
          end
        end
      end

      # PATCH/PUT /leasings/1
      # PATCH/PUT /leasings/1.json
      def update
        respond_to do |format|
          if @leasing.update(leasing_params) && leasing_params["approved"] = false
            format.html { redirect_to ad_leasing_path(@leasing), notice: 'Leasing was successfully updated.' }
            format.json { render :show, status: :ok, location: @leasing }
          elsif leasing_params["approved"] = true
            @bill = Bill.new()
            @bill.ht =(@leasing.end_date.to_date -  @leasing.beginning_date.to_date).to_i * @leasing.vehicle.price
            @bill.ttc = @bill.ht * 1.20
            fidelity = (@leasing.user.fidelity.to_f / 100).to_f * @bill.ttc.to_f
            @bill.total = @bill.ttc.to_f - fidelity
            @bill.deadline = @leasing.beginning_date + 30
            @bill.leasing_id = @leasing.id
            @bill.paid = false
            @bill.num_bill = Bill.last.num_bill + 1
            if @leasing.update(leasing_params) && @bill.save
              format.html { redirect_to ad_bill_path(@bill), notice: 'La facture a etait créé' }
              format.json { render :show, status: :ok, location: @leasing }
              end
          else
            format.html { render :edit }
            format.json { render json: @leasing.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /leasings/1
      # DELETE /leasings/1.json
      def destroy
        @leasing.destroy
        respond_to do |format|
          format.html { redirect_to leasings_url, notice: 'Leasing was successfully destroyed.' }
          format.json { head :no_content }
        end
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_leasing
          @leasing = Leasing.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def leasing_params
          params.fetch(:leasing).permit(
                             :beginning_date,
                             :end_date,
                             :limit_km,
                             :penelty,
                             :user_id,
                             :vehicle_id
          )
        end
    end
end
