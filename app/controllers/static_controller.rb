class StaticController < ApplicationController
  def home
    @vehicle = Vehicle.order(:created_at).limit(4)
  end
end