class LeasingsController < ApplicationController
  before_action :set_leasing, only: [:show, :edit, :update, :destroy, :louer]
  before_action :authenticate_user!
  # GET /leasings
  # GET /leasings.json
  def index
    @leasings = Leasing.all
  end

  # GET /leasings/1
  # GET /leasings/1.json
  def show
  end

  # GET /leasings/new
  def new
    @vehicle = Vehicle.find(params[:id])
    @leasing = Leasing.new()
    @leasing.user_id = current_user.id
    @leasing.vehicle_id = @vehicle.id
  end

  # GET /leasings/1/edit
  def edit
  end

    # POST /leasings
  # POST /leasings.json
  def create
    @leasing = Leasing.new(leasing_params)
    numJour = @leasing.end_date - @leasing.beginning_date
    numkm = numJour * 1000
    @leasing.limit_km =  numkm
    @leasing.day =  numJour
    respond_to do |format|
      if @leasing.save
        format.html { redirect_to root_url, notice: 'Votre location est en cour de traitement.' }
        format.json { render :show, status: :created, location: @leasing }
      else
        format.html { render :new }
        format.json { render json: @leasing.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /leasings/1
  # PATCH/PUT /leasings/1.json
  def update
    respond_to do |format|
      if @leasing.update(leasing_params)
        format.html { redirect_to @leasing, notice: 'Leasing was successfully updated.' }
        format.json { render :show, status: :ok, location: @leasing }
      else
        format.html { render :edit }
        format.json { render json: @leasing.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /leasings/1
  # DELETE /leasings/1.json
  def destroy
    @leasing.destroy
    respond_to do |format|
      format.html { redirect_to leasings_url, notice: 'Leasing was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_leasing
      @leasing = Leasing.find(params[:id])
    end

  def leasing_params
    params.fetch(:leasing).permit(
      :beginning_date,
      :end_date,
      :limit_km,
      :penelty,
      :user_id,
      :vehicle_id,
      :approved,
      :bill_id
    )
  end
end
