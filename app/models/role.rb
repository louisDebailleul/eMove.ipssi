# frozen_string_literal: true

#
class Role < ApplicationRecord
  scope :registrable, -> { where(name: %w[admin client]) }
end
