class Vehicle < ApplicationRecord
  has_many :leasings
  belongs_to :model
  belongs_to :type

  mount_uploader :photo, PhotoUploader


  def name_model
    "#{self.model.name}"
  end
  #donne le nombre de model par page
  self.per_page = 20
end
