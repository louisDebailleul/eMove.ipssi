class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :registerable,
         :recoverable,
         :rememberable,
         :trackable,
         :validatable,
         :confirmable

  has_many :leasings

  belongs_to :role
  ADMIN = 1
  def admin?
    role_id == ADMIN
  end



  #donne le nombre de model par page
  self.per_page = 20

  validates_format_of :email, with: Devise.email_regexp

  # validates_format_of :first_name, {:with => /\[a-z0-9\-]+/}

  validates_presence_of(
   :first_name,
   :last_name,
   :phone,
   :number_permie,
   :password,
   :password_confirmation,
   :street,
   :city,
   :zipcode,
   :fidelity
  )
end
