class Model < ApplicationRecord
  belongs_to :brand
  has_many :vehicles
  #donne le nombre de model par page
  self.per_page = 20
end
