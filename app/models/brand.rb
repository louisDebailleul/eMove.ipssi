class Brand < ApplicationRecord
  has_many :models



  validates_presence_of(
  :name,
  :logo,
  :history
  )

  #donne le nombre de model par page
  self.per_page = 20
  mount_uploader :logo, PhotoUploader
end
