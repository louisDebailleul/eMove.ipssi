class Leasing < ApplicationRecord
  belongs_to :user
  belongs_to :vehicle
  has_many :bills

  #donne le nombre de model par page
  self.per_page = 20
end
