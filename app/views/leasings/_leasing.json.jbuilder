json.extract! leasing, :id, :created_at, :updated_at
json.url leasing_url(leasing, format: :json)
