# frozen_string_literal: true

# Pundit
class UserPolicy < ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def show?
    user
  end

  def update?
    user
  end

  def edit?
    update?
  end

  def scope
    Pundit.policy_scope!(user, record.class)
  end
end
